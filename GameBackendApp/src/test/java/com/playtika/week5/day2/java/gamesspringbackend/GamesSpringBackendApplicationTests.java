package com.playtika.week5.day2.java.gamesspringbackend;

import com.playtika.week5.day2.java.gamesspringbackend.config.GamesSettings;
import com.playtika.week5.day2.java.gamesspringbackend.controllers.PlayerController;
import com.playtika.week5.day2.java.gamesspringbackend.models.Player;
import com.playtika.week5.day2.java.gamesspringbackend.services.PlayerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.ResponseEntity;

import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class GamesSpringBackendApplicationTests {

    private static final String BASE_URL = "/api/info";

    @LocalServerPort
    private int serverPort;

    @Autowired
    PlayerService playerService;

    @Autowired
    TestRestTemplate testRestTemplate;

    @Test
    void contextLoads() {
    }

    @Test
    void testPlayerControllerIsUp() {
        String fullUrl = "http://127.0.0.1:" + serverPort + BASE_URL;
        ResponseEntity<GamesSettings> responseEntity = testRestTemplate.getForEntity(fullUrl, GamesSettings.class);
        assertThat(responseEntity.getStatusCodeValue()).isEqualTo(200);
    }

    @Test
    void testPlayerControllerData() {
        String fullUrl = "http://127.0.0.1:" + serverPort + BASE_URL;
        ResponseEntity<GamesSettings> responseEntity = testRestTemplate.getForEntity(fullUrl, GamesSettings.class);
        assertThat(responseEntity.getBody().getVersion()).isEqualTo("1.0.0");
    }


}
