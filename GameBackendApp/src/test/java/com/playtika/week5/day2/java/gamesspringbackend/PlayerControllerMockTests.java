package com.playtika.week5.day2.java.gamesspringbackend;

import com.playtika.week5.day2.java.gamesspringbackend.models.Player;
import com.playtika.week5.day2.java.gamesspringbackend.services.PlayerService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultHandlers;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
public class PlayerControllerMockTests {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    PlayerService playerService;

    @Test
    public void test() throws Exception {
        ArrayList<Player> players = new ArrayList<Player>();
        players.add(new Player());
        players.add(new Player());
        when(playerService.findAll()).thenReturn(new ArrayList<>());

        mockMvc.perform(get("/api/players"))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].fullName").value(""));
    }
}
