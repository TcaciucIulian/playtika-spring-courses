package com.playtika.week5.day2.java.gamesspringbackend.models;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "players")
public class Player {

    public enum PlayerGender {
        MALE, FEMALE, NOT_DISCLOSED
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "player_id")
    private long id;

    @Column(name = "player_fullname")
    private String fullName;

    @Column(name = "player_age")
    private int age;

    @Column(name = "player_isonline")
    private boolean isOnline;

    @Column(name = "player_gender")
    @Enumerated(EnumType.STRING)
    private PlayerGender gender;

    @Column(name = "player_birthdate")
    @Temporal(TemporalType.DATE)
    private Date birthdate;

    @OneToOne(mappedBy = "player", cascade = CascadeType.ALL)
    private BillingAddress billingAddress;

    @ManyToMany
    @JoinTable(name = "player_games",
            joinColumns = @JoinColumn(name = "player_id"),
            inverseJoinColumns = @JoinColumn(name = "game_id"))
    private List<Game> games;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public boolean isOnline() {
        return isOnline;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    public PlayerGender getGender() {
        return gender;
    }

    public void setGender(PlayerGender gender) {
        this.gender = gender;
    }

    public Date getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(Date birthdate) {
        this.birthdate = birthdate;
    }

    public BillingAddress getBillingAddress() {
        return billingAddress;
    }

    public void setBillingAddress(BillingAddress billingAddress) {
        this.billingAddress = billingAddress;
    }

    public List<Game> getGames() {
        return new ArrayList<>(games);
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }
}
