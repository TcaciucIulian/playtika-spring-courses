package com.playtika.week5.day2.java.gamesspringbackend.services;

import com.playtika.week5.day2.java.gamesspringbackend.models.BillingAddress;
import com.playtika.week5.day2.java.gamesspringbackend.models.Game;
import com.playtika.week5.day2.java.gamesspringbackend.models.Player;
import com.playtika.week5.day2.java.gamesspringbackend.repositories.BillingAddressRepository;
import com.playtika.week5.day2.java.gamesspringbackend.repositories.GameRepository;
import com.playtika.week5.day2.java.gamesspringbackend.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    BillingAddressRepository billingAddressRepository;

    @Autowired
    GameRepository gameRepository;

    public List<Player> findAll() {
        return this.playerRepository.findAll();
    }

    public Player addPlayer(Player player) {
        return this.playerRepository.saveAndFlush(player);
    }

    public Player addBillingAddress(long playerId, BillingAddress billingAddress) {
        Optional<Player> playerOptional = this.playerRepository.findById(playerId);
        if(playerOptional.isPresent()) {
            Player player = playerOptional.get();
//            BillingAddress newBillingAddress = this.billingAddressRepository.saveAndFlush(billingAddress);
            billingAddress.setPlayer(player);
            player.setBillingAddress(billingAddress);
            this.playerRepository.saveAndFlush(player);
            return player;
        } else {
            return null;
        }
    }

    public Player addGame(long playerId, long gameId) {
        Optional<Player> playerOptional = this.playerRepository.findById(playerId);
        Optional<Game> gameOptional = this.gameRepository.findById(gameId);

        if(playerOptional.isPresent() && gameOptional.isPresent()) {
            Player player = playerOptional.get();
            player.getGames().add(gameOptional.get());
            this.playerRepository.saveAndFlush(player);
            return player;
        } else {
            return null;
        }
    }

    public List<Player> getFemalePlayers() {
        return this.playerRepository.getFemalePlayers();
    }

    public List<Player> getMalePlayers() {
        return this.playerRepository.getMalePlayers();
    }

    //  -> /api/players/find?gender=MALE&age
    public List<Player> getPlayerByAgeAndGender(String gender, int minAge) {
        return this.playerRepository.getPlayersByAgeAndGender(gender, minAge);
    }

    public Page<Player> getPlayersPage(Pageable pageable) {
        return this.playerRepository.findAll(pageable);
    }


}
