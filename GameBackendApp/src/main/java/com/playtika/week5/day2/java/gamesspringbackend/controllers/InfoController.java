package com.playtika.week5.day2.java.gamesspringbackend.controllers;

import com.playtika.week5.day2.java.gamesspringbackend.config.GamesSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/info") // pot fi combinate
public class InfoController {

    @Autowired // injectam
    GamesSettings gamesSettings;

    @GetMapping
    public GamesSettings getSettings(){
        return this.gamesSettings;
    }

}
