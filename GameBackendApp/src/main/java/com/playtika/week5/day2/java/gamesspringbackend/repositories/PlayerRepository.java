package com.playtika.week5.day2.java.gamesspringbackend.repositories;

import com.playtika.week5.day2.java.gamesspringbackend.models.Player;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface PlayerRepository extends JpaRepository<Player, Long> {

    @Query(value = "SELECT * FROM players p  WHERE p.player_gender = 'FEMALE'", nativeQuery = true)
    List<Player> getFemalePlayers();

    @Query(value = "SELECT * FROM players p  WHERE p.player_gender = 'MALE'", nativeQuery = true)
    List<Player> getMalePlayers();

    @Query(value = "SELECT * FROM players p  WHERE p.player_gender = :gender AND p.player_age > :age", nativeQuery = true)
    List<Player> getPlayersByAgeAndGender(String gender, int age);
}
