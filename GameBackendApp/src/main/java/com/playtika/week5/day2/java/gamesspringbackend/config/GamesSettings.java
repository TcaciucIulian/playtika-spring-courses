package com.playtika.week5.day2.java.gamesspringbackend.config;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Component // este un bean
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
@PropertySource("classpath:games.properties") // se duce in resourses cu "classpath"
public class GamesSettings {

    @Value("${app.version:0.0.0}")
    private String version;

    @Value("${description}")
    private String description;

    @Value("${no.max.players}")
    private int maxPlayers;

    @Value("${developers}")
    private ArrayList<String> developers;

    @Value("${ports}")
    private ArrayList<Integer> ports;

    @Value("#{${contribution}}")
    private Map<String, Integer> contributionMap;


    public String getVersion() {
        return version;
    }

    public String getDescription() {
        return description;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public ArrayList<String> getDevelopers() {
        return (ArrayList<String>) developers.clone();
    }


    public ArrayList<Integer> getPorts() {
        return (ArrayList<Integer>) ports.clone();
    }

    public Map<String, Integer> getContributionMap() {
        return new HashMap<>(this.contributionMap);
    }
}
