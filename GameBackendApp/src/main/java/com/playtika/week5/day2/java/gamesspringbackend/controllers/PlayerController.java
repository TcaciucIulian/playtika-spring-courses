package com.playtika.week5.day2.java.gamesspringbackend.controllers;


import com.playtika.week5.day2.java.gamesspringbackend.models.BillingAddress;
import com.playtika.week5.day2.java.gamesspringbackend.models.Player;
import com.playtika.week5.day2.java.gamesspringbackend.services.PlayerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/players")
public class PlayerController {

    @Autowired
    PlayerService playerService;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @GetMapping
    public List<Player> findAll() {
        return this.playerService.findAll();
    }

    @PostMapping
    public Player addNewPlayer(@RequestBody Player player) {
        return this.playerService.addPlayer(player);
    }

    @PostMapping
    @RequestMapping("/{id}/add/billing")
    public @ResponseBody Player addBillingAddress(@PathVariable long id, @RequestBody BillingAddress billingAddress) {
        return this.playerService.addBillingAddress(id, billingAddress);
    }

    //      /api/players/1/addgame?gameId=1;
    @PostMapping
    @RequestMapping(value = "/{playerId}/addgame", params = {"gameId"})
    public Player addGame(@PathVariable long playerId, @RequestParam long gameId){
        return this.playerService.addGame(playerId, gameId);
    }

    //  api/players/addgame?gameId=1&playerId=2
    @PostMapping
    @RequestMapping(value = "/addgame", params = {"gameId", "playerId"})
    public Player addGameWithParams(@PathVariable long playerId, @RequestParam long gameId){
        return this.playerService.addGame(playerId, gameId);
    }

    @GetMapping
    @RequestMapping("/female")
    public List<Player> getFemalePlayers() {
        return this.playerService.getFemalePlayers();
    }

    @GetMapping
    @RequestMapping("/male")
    public List<Player> getMalePlayers() {
        return this.playerService.getMalePlayers();
    }

    @GetMapping(params = {"gender", "minAge"})
    @RequestMapping("/find")
    public List<Player> getPlayersByAgeAndGender(@RequestParam String gender, @RequestParam int minAge) {
        return this.playerService.getPlayerByAgeAndGender(gender, minAge);
    }

    @GetMapping
    @RequestMapping("/page/{pageId}")
    public List<Player> getPlayersPage(@PathVariable int pageId) {
        Pageable pageable = PageRequest.of(pageId, 2);
        return this.playerService.getPlayersPage(pageable).toList();
    }


    // http://127.0.0.1/api/players/page?page=0&size=3&sort=age;
    @GetMapping
    @RequestMapping("/page")
    public List<Player> getPlayerPage(Pageable pageable) {
        return this.playerService.getPlayersPage(pageable).toList();
    }

    @GetMapping(params = {"nameFilter"})
    @RequestMapping("/count")
    public String getPlayersNumberByAge(@RequestParam String nameFilter) {
        Logger logger = LoggerFactory.getLogger(PlayerController.class);

        String query = String.format("SELECT COUNT(player_id) FROM players WHERE player_fullname LIKE '%s'", nameFilter);
        logger.info(query);

        jdbcTemplate.execute(query);
        return "OK";
    }
}
