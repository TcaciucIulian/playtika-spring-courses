package com.playtika.week5.day2.java.gamesspringbackend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GamesSpringBackendApplication {

    public static void main(String[] args) {
        SpringApplication.run(GamesSpringBackendApplication.class, args);
    }

}
