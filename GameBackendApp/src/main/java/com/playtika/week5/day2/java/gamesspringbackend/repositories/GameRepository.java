package com.playtika.week5.day2.java.gamesspringbackend.repositories;

import com.playtika.week5.day2.java.gamesspringbackend.models.Game;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GameRepository extends JpaRepository<Game, Long> {
}
