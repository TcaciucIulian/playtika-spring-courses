package com.playtika.week5.day2.java.gamesspringbackend.repositories;

import com.playtika.week5.day2.java.gamesspringbackend.models.BillingAddress;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BillingAddressRepository extends JpaRepository<BillingAddress, Long> {
}
