package com.playtika.springbootapp.controllers;

import com.playtika.springbootapp.models.dto.AppInfo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.annotation.HttpMethodConstraint;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/info")
public class InfoController {

    @Value("${app.name}")
    public String appName;

    @Value("{app.version:2.0.0}")
    public String appVersion;

    @GetMapping
    public AppInfo getInfo() {
        return new AppInfo(appName, appVersion);
    }


    @GetMapping
    @RequestMapping("/all")
    public List<AppInfo> getAll() {
        List<AppInfo> infoList = new ArrayList<>();
        infoList.add(new AppInfo("Hello", "1"));
        infoList.add(new AppInfo("World", "2"));

        return infoList;

    }

}
