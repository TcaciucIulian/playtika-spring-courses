package com.playtika.springbootapp.controllers;


import com.playtika.springbootapp.exceptions.BadRequestException;
import com.playtika.springbootapp.models.dto.Player;
import com.playtika.springbootapp.services.PlayersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/players")
public class PlayersController {

    @Autowired
    PlayersService playersService;

    @GetMapping
    public List<Player> getAll() {
        return this.playersService.getAll();
    }

    @GetMapping
    @RequestMapping("/{id}")
    public Player getPlayerById(@PathVariable long id) throws BadRequestException {
        try {
            Player player = this.playersService.getPlayerById(id);
            return player;

        } catch (Exception ex) {
            throw new BadRequestException();
        }
    }

    @ExceptionHandler(BadRequestException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public @ResponseBody String exceptionHandler(BadRequestException exception) {
        return "Bad Request. Invalid id -> player not found";
    }
}
