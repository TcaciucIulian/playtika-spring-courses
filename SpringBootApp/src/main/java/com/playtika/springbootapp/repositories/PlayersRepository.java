package com.playtika.springbootapp.repositories;

import com.playtika.springbootapp.models.dto.Player;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlayersRepository extends JpaRepository<Player, Long> {
    // we get all CRUD OPERATIONS

}
