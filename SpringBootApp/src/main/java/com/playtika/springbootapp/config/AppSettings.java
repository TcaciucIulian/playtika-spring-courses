package com.playtika.springbootapp.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;


@Component
public class AppSettings {
    @Value("${app.name}")
    public String appName;

    @Value("${app.version:2.0.0}")
    public String appVersion;


}
