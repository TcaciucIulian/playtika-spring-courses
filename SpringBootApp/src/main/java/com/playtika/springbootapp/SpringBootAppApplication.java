package com.playtika.springbootapp;

import com.playtika.springbootapp.config.AppSettings;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAppApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootAppApplication.class, args);

        AppSettings appSettings = new AppSettings();
        System.out.println(appSettings.appName + " " + appSettings.appVersion);
    }

}
