package com.playtika.springbootapp.services;

import com.playtika.springbootapp.models.dto.Player;
import com.playtika.springbootapp.repositories.PlayersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class PlayersService {

    @Autowired
    PlayersRepository playersRepository;

    public List<Player> getAll() {
        return playersRepository.findAll();
    }

    public Player getPlayerById(long id) {
        return playersRepository.findById(id).get();
    }

}
