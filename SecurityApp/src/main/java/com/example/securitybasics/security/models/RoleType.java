package com.example.securitybasics.security.models;

import org.springframework.security.core.GrantedAuthority;

public enum RoleType  {
    ROLE_ADMIN, ROLE_USER;
}
