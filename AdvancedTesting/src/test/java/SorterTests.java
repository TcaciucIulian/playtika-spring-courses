import exceptions.SortException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;

import static org.junit.Assert.assertArrayEquals;

@RunWith(Parameterized.class)
public class SorterTests {

    private int[] input;
    private int[] output;

    public SorterTests(int[] input, int[] output) {
        this.input = input;
        this.output = output;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> getTestData() {
        return Arrays.asList(new Object[][]{
                {new int[]{1, 2, 3}, new int[]{1, 2, 3}},
                {new int[]{3, 2, 1}, new int[]{1, 2, 3}},
                {new int[]{5, 6, 4}, new int[]{4, 5, 6}}
        });
    }

    @Test
    public void testArraySorter() throws SortException {
        int[] actualOutput = Sorter.sort(input);
        assertArrayEquals("The array is not correctly sorted", output, actualOutput);
    }
}
