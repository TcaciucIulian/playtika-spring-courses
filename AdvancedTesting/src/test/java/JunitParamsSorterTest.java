import exceptions.SortException;
import junitparams.JUnitParamsRunner;
import junitparams.Parameters;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.assertj.core.api.Assertions.*;
import static org.junit.Assert.*;

@RunWith(JUnitParamsRunner.class)
public class JunitParamsSorterTest {
    public Object[] getData() {
        return new Object[][] {
                {
                    new int[]{1, 2, 3},
                    new int[]{1, 2, 3}
                },
                {
                    new int[]{3, 2, 1},
                    new int[]{1, 2, 3}
                },
                {
                    new int[]{5, 4, 6},
                    new int[]{4, 5, 6}
                }
        };
    }

    @Test
    @Parameters(method = "getData")
    public void testArraySorter(int[] input, int[] output) throws SortException {
        int[] actualOutput = Sorter.sort(input);
        assertArrayEquals("The sort method doesn't work", output, actualOutput);
    }


    @Test(expected = SortException.class)
    public void testNullArray() throws SortException {
        Sorter.sort(null);
    }

    @Test
    public void testNullArray2() {
        Exception exception = assertThrows(SortException.class, () -> Sorter.sort(null));
        assertEquals("array is null", exception.getMessage());
    }

    @Test
    public void testNullArray3() {
        assertThatExceptionOfType(SortException.class)
                .isThrownBy(() -> Sorter.sort(null))
                .withMessage("array is null");
    }
}
