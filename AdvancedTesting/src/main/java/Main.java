import exceptions.SortException;

public class Main {

    public static void main(String[] args) {
        int[] array = {4, 1, 2, 3};
        try {
            int[] sortedArray = Sorter.sort(array);
        } catch (SortException e) {
            e.printStackTrace();
        }
        try {
            Sorter.sort(null);
        } catch (SortException e) {
            e.printStackTrace();
        }
    }
}
