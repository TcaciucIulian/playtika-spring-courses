import exceptions.SortException;

import java.util.Arrays;

public class Sorter {

    public static int[] sort(int[] array) throws SortException {
        if(array == null) {
            throw new SortException("arays is null");
        }
        int[] copy = Arrays.copyOf(array, array.length);
        Arrays.sort(copy);
        return copy;
    }
}
