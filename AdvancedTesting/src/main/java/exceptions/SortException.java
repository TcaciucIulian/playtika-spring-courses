package exceptions;

public class SortException extends Exception{

    public SortException() {
        super();
    }

    public SortException(String message) {
        super(message);
    }
}
