package com.playtika.java.training.week3.day3;

public class InterchangeSort implements Sortable{

    public void interchangeSort(int[] values) {
        System.out.println("Sorting with interchange algorithm");
    }

    @Override
    public void sort(int[] values) {
        this.interchangeSort(values);
    }
}
