package com.playtika.java.training.week3.day3;

public interface Sortable {

    public void sort(int[] values);

}
