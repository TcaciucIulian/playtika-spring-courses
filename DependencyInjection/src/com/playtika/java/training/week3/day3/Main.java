package com.playtika.java.training.week3.day3;

public class Main {
// main este IoC container pentru ca el zice ce sa apeleze
    public static void main(String[] args) {
	    MyData data = new MyData();
//	      data.sort();
//        data.sort(1);
//        data.sort(2);

        data.sort(new BubbleSort());
        data.sort(new InterchangeSort());

        MyData myData = new MyData(new InterchangeSort());
        myData.sort();
    }
}
