package com.playtika.java.training.week3.day3;

public class MyData {

    int[] values;
    Sortable mySortAlgorithm = null;

    public MyData() {
        this.mySortAlgorithm = new BubbleSort();
    }


    // constructor injection
    public MyData(Sortable sortable) {
        this.mySortAlgorithm = sortable;
    }


    // setter injection
    public void setMySortAlgorithm(Sortable mySortAlgorithm) {
        this.mySortAlgorithm = mySortAlgorithm;
    }


    //    version 1
//    public void sort() {
//        System.out.println("Bubble sorting the values");
//
//    }

//    version 2
//    public void sort() {
//        BubbleSort bubbleSort = new BubbleSort();
//        bubbleSort.bubbleSort(values);
//    }


        //version 3
//    public void sort(int type) {
//        if(type == 1) {
//            BubbleSort bubbleSort = new BubbleSort();
//            bubbleSort.bubbleSort(values);
//        }
//
//        if(type == 2) {
//            InterchangeSort interchangeSort = new InterchangeSort();
//            interchangeSort.interchangeSort(values);
//        }
//    }


    // version 4
    public void sort(Sortable sortable) {
        sortable.sort(values);
    }


    // version 5
    public void sort() {
        if(this.mySortAlgorithm != null) {
            this.mySortAlgorithm.sort(values);
        } else {
            throw new UnsupportedOperationException();
        }
    }
}
