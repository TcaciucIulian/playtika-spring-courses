package com.example.securitybasics.security.repositories;

import com.example.securitybasics.security.models.Role;
import com.example.securitybasics.security.models.RoleType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepository extends JpaRepository<Role, Long> {

    Role findByName(String name);

    @Override
    void delete(Role role);
}
