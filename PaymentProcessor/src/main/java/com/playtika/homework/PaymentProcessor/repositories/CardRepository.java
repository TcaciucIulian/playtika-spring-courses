package com.playtika.homework.PaymentProcessor.repositories;

import com.playtika.homework.PaymentProcessor.models.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CardRepository extends JpaRepository<Card, Long> {

    @Query(value = "SELECT * FROM cards WHERE cards.card_number = :cardNumber", nativeQuery = true)
    Card getCardByCardNumber(long cardNumber);

    @Query(value = "SELECT * FROM cards WHERE cards.id = :userId", nativeQuery = true)
    List<Card> getCardsByUserId(long userId);

    @Query(value = "SELECT id FROM cards WHERE cards.id = :cardId", nativeQuery = true)
    long getUserIdByCardId(long cardId);
}
