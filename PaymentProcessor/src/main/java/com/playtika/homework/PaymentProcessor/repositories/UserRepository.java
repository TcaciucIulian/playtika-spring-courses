package com.playtika.homework.PaymentProcessor.repositories;

import com.playtika.homework.PaymentProcessor.models.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    @Query(value = "SELECT * FROM users WHERE users.user_cnp = :CNP", nativeQuery = true)
    User getUserByCNP(long CNP);

}
