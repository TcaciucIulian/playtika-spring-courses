package com.playtika.homework.PaymentProcessor.services;

import com.playtika.homework.PaymentProcessor.models.Report;
import com.playtika.homework.PaymentProcessor.models.User;
import com.playtika.homework.PaymentProcessor.repositories.ReportRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ReportService {
    @Autowired
    ReportRepository reportRepository;

    public Report addReport(Report report) {
        return this.reportRepository.saveAndFlush(report);
    }
}
