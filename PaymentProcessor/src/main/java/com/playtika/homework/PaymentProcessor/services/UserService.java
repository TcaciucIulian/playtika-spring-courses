package com.playtika.homework.PaymentProcessor.services;

import com.playtika.homework.PaymentProcessor.models.Address;
import com.playtika.homework.PaymentProcessor.models.Card;
import com.playtika.homework.PaymentProcessor.models.User;
import com.playtika.homework.PaymentProcessor.repositories.CardRepository;
import com.playtika.homework.PaymentProcessor.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.print.DocFlavor;
import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    CardRepository cardRepository;

    public User addUser(User user) {
        return this.userRepository.saveAndFlush(user);
    }

    public List<User> getAllUsers() {
        return this.userRepository.findAll();
    }

    public boolean updateUser(User user) {
        Optional<User> existingUser = this.userRepository.findById(user.getId());
        if(existingUser.isPresent()) {
            this.userRepository.saveAndFlush(user);
            return true;
        }

        return false;
    }

    public boolean deleteUser(long id) {
        if(this.userRepository.findById(id).isPresent()) {
            this.userRepository.deleteById(id);
            return true;
        }

        return false;
    }

    public User findById(long id) {
        Optional<User> user = this.userRepository.findById(id);
        if(user.isPresent()) {
            return user.get();
        }

        return null;
    }

    public User addAddress(long userId, Address address) {
        Optional<User> userOptional = this.userRepository.findById(userId);
        if(userOptional.isPresent()) {
            User user = userOptional.get();
            address.setUser(user);
            user.setAddress(address);
            this.userRepository.saveAndFlush(user);
            return user;
        }

        return null;
    }

    public User addCard(long userId, long cardId) {
        Optional<User> userOptional = this.userRepository.findById(userId);
        Optional<Card> cardOptional = this.cardRepository.findById(cardId);

        if(userOptional.isPresent() && cardOptional.isPresent()) {
            User user = userOptional.get();
            user.getCards().add(cardOptional.get());
            this.userRepository.saveAndFlush(user);
            return user;
        }

        return null;
    }

    public User getUserByCNP(long CNP) {
        return this.userRepository.getUserByCNP(CNP);
    }

}
