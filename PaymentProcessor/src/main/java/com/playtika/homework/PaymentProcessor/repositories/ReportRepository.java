package com.playtika.homework.PaymentProcessor.repositories;

import com.playtika.homework.PaymentProcessor.models.Report;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReportRepository extends JpaRepository<Report, Long> {
}
