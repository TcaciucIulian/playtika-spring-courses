package com.playtika.homework.PaymentProcessor.controllers;

import com.playtika.homework.PaymentProcessor.models.Card;
import com.playtika.homework.PaymentProcessor.models.User;
import com.playtika.homework.PaymentProcessor.services.CardService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/users")
public class CardController {

    @Autowired
    CardService cardService;

    @PostMapping
    @RequestMapping(value = "/{cardId}/addtransaction", params = {"transactionId"})
    public Card addCard(@PathVariable long cardId, @RequestParam long transactionId){
        return this.cardService.addCard(cardId, transactionId);
    }
}
