package com.playtika.homework.PaymentProcessor.services;

import com.playtika.homework.PaymentProcessor.models.Card;
import com.playtika.homework.PaymentProcessor.models.Transaction;
import com.playtika.homework.PaymentProcessor.models.User;
import com.playtika.homework.PaymentProcessor.repositories.CardRepository;
import com.playtika.homework.PaymentProcessor.repositories.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardService {

    @Autowired
    CardRepository cardRepository;

    @Autowired
    TransactionRepository transactionRepository;

    public Card getCardById(long id) {
        Optional<Card> card = this.cardRepository.findById(id);
        if(card.isPresent()) {
            return card.get();
        }

        return null;
    }

    public Card addCard(long cardId, long transactionId) {
        Optional<Transaction> transactionOptional = this.transactionRepository.findById(transactionId);
        Optional<Card> cardOptional = this.cardRepository.findById(cardId);

        if(transactionOptional.isPresent() && cardOptional.isPresent()) {
            Card card = cardOptional.get();
            card.getTransactions().add(transactionOptional.get());
            this.cardRepository.saveAndFlush(card);
            return card;
        }

        return null;
    }

    public Card getCardByCardNumber(long cardNumber) {
        return this.cardRepository.getCardByCardNumber(cardNumber);
    }

    public List<Card> getCardsByUserId(long userId) {
        return this.cardRepository.getCardsByUserId(userId);
    }

    public long getUserIdByCardId(long cardId) {
        return this.cardRepository.getUserIdByCardId(cardId);
    }
}
