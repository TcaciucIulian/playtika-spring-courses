package com.playtika.homework.PaymentProcessor.models;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.exception.DataException;

import javax.persistence.*;
import javax.print.attribute.standard.MediaSize;
import java.util.Date;

@Entity
@Table(name = "transactions")
public class Transaction {

    public enum Status {
        INITIATED, AUTHORIZED, CAPTURED, REFUNDED, CANCEL
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "transaction_id")
    private long transactionId;

    @Column(name = "transaction_date")
    @Temporal(TemporalType.DATE)
    private Date transcationDate;

    @Column(name = "transaction_status")
    @Enumerated(EnumType.STRING)
    private Status status;

    @Column(name = "transaction_amount")
    private double amount;

    @Column(name = "transaction_captured_date")
    @Temporal(TemporalType.DATE)
    private Date captureDate;

    @Column(name = "transaction_refund_date")
    @Temporal(TemporalType.DATE)
    private Date refundDate;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "card_id", insertable = false, nullable = false, updatable = false)
    private Card card;

    public long getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(long transactionId) {
        this.transactionId = transactionId;
    }

    public Date getTranscationDate() {
        return transcationDate;
    }

    public void setTranscationDate(Date transcationDate) {
        this.transcationDate = transcationDate;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Date getCaptureDate() {
        return captureDate;
    }

    public void setCaptureDate(Date captureDate) {
        this.captureDate = captureDate;
    }

    public Date getRefundDate() {
        return refundDate;
    }

    public void setRefundDate(Date refundDate) {
        this.refundDate = refundDate;
    }

    public Card getCard() {
        return card;
    }

    public void setCard(Card card) {
        this.card = card;
    }


}
