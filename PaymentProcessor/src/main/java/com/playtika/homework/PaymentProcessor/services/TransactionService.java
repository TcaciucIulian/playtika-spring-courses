package com.playtika.homework.PaymentProcessor.services;

import com.playtika.homework.PaymentProcessor.models.Card;
import com.playtika.homework.PaymentProcessor.models.Report;
import com.playtika.homework.PaymentProcessor.models.Transaction;
import com.playtika.homework.PaymentProcessor.models.User;
import com.playtika.homework.PaymentProcessor.repositories.CardRepository;
import com.playtika.homework.PaymentProcessor.repositories.ReportRepository;
import com.playtika.homework.PaymentProcessor.repositories.TransactionRepository;
import com.playtika.homework.PaymentProcessor.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.LinkedBlockingDeque;

@Service
public class TransactionService {


    @Autowired
    CardService cardService;

    @Autowired
    TransactionRepository transactionRepository;

    @Autowired
    UserService userService;

    @Autowired
    ReportService reportService;

//
//    public long initializeTransaction(long userId, long cardId) {
//        int noTransactions = this.transactionRepository.findAll().size();
//        Transaction transaction = new Transaction();
//        transaction.setTransactionId(noTransactions);
//        transaction.setStatus(Transaction.Status.INITIATED);
//
//        Card userCard = this.cardService.getCardsByUserId(userId).get((int)cardId);
//        userCard.getTransactions().add(transaction);
//
//        return userId;
//    }

    public Transaction authorizeTransaction(long transactionId) {
        Optional<Transaction> existingTransaction = this.transactionRepository.findById(transactionId);
        if(existingTransaction.isPresent()) {
            Transaction transaction = existingTransaction.get();
            long cardIdForThisTransaction = this.transactionRepository.getCardIdForTransaction(transaction.getTransactionId());
            long userIdForThisCardId = this.cardService.getUserIdByCardId(cardIdForThisTransaction);
            User user = this.userService.findById(userIdForThisCardId);
            if(user != null && transaction.getStatus().equals(Transaction.Status.INITIATED)) {
                Report report = new Report();
                Card card = this.cardService.getCardById(cardIdForThisTransaction);
                report.setAmount(transaction.getAmount());
                report.setCardNumber(card.getCardNumber());
                report.setHolderName(card.getHolderName());
                this.reportService.addReport(report);
                transaction.setStatus(Transaction.Status.AUTHORIZED);
                this.transactionRepository.saveAndFlush(transaction);
            } else {
                return null;
            }

            return transaction;
        }

        return null;
    }

    public Transaction captureTransaction(long transactionId) {
        Optional<Transaction> existingTransaction = this.transactionRepository.findById(transactionId);
        if(existingTransaction.isPresent()) {
            Transaction transaction = existingTransaction.get();
            if(transaction.getStatus().equals(Transaction.Status.AUTHORIZED)) {
                transaction.setStatus(Transaction.Status.CAPTURED);
            }

            this.transactionRepository.saveAndFlush(transaction);
            return transaction;
        }

        return null;
    }

    public Transaction refundTransaction(long transactionId) {
        Optional<Transaction> existingTransaction = this.transactionRepository.findById(transactionId);
        if(existingTransaction.isPresent()) {
            Transaction transaction = existingTransaction.get();
            if(transaction.getStatus().equals(Transaction.Status.CAPTURED)) {
                transaction.setStatus(Transaction.Status.REFUNDED);
            }

            this.transactionRepository.saveAndFlush(transaction);
            return transaction;
        }

        return null;
    }

    public Transaction cancelTransaction(long transactionId) {
        Optional<Transaction> existingTransaction = this.transactionRepository.findById(transactionId);
        if(existingTransaction.isPresent()) {
            Transaction transaction = existingTransaction.get();
            if(transaction.getStatus().equals(Transaction.Status.AUTHORIZED)) {
                transaction.setStatus(Transaction.Status.CANCEL);
            }

            if(transaction.getStatus().equals(Transaction.Status.INITIATED)) {
                transaction.setStatus(Transaction.Status.CANCEL);
            }

            this.transactionRepository.saveAndFlush(transaction);
            return transaction;
        }

        return null;
    }


    public Transaction getTransactionById(long transactionId) {
        Optional<Transaction> transaction = this.transactionRepository.findById(transactionId);
        if(transaction.isPresent()) {
            return transaction.get();
        }

        return null;
    }

    public List<Transaction> getTransactionsByStatus(String status) {
        return this.transactionRepository.getTransactionsByStatus(status);
    }

    public List<Transaction> getTransactionsByCardNumber(long cardNumber) {
        Card card = this.cardService.getCardByCardNumber(cardNumber);
        return this.transactionRepository.getTransactionsByCardNumber(card.getId());
    }

    public List<List<Transaction>> getTransactionByUserCnp(long CNP) {
        User user = this.userService.getUserByCNP(CNP);
        List<Card> cards = this.cardService.getCardsByUserId(user.getId());
        List<List<Transaction>> transactions = new ArrayList<>();
        for(int i = 0; i < cards.size(); i++) {
            transactions.add(this.transactionRepository.getTransactionsByUserCNP(cards.get(i).getId()));
        }

        return transactions;
    }

    public List<Transaction> getTransactionInDateInterval(String startDate, String endDate) {
        return this.transactionRepository.getTransactionInDateInterval(startDate, endDate);
    }

    public long getCardIdForTransaction(long transactionId) {
        return this.transactionRepository.getCardIdForTransaction(transactionId);
    }
}
