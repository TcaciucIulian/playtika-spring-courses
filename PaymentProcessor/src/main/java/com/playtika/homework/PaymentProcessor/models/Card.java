package com.playtika.homework.PaymentProcessor.models;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Table(name = "cards")
public class Card {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id")
    private long id;

    @Column(name = "card_number")
    private long cardNumber;

    @JsonIgnore
    @Transient
    @Column(name = "card_holdername")
    private String holderName;

    @Column(name = "card_expire_date")
    @Temporal(TemporalType.DATE)
    private Date expireDate;

    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "user_id", insertable = false, updatable = false, nullable = false)
    private User user;

    @OneToMany(mappedBy = "card", cascade = CascadeType.ALL)
    private List<Transaction> transactions;


    @PostLoad
    public void initHolderName() {
        this.holderName = user.getFirstName() + " " + user.getLastName();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getCardNumber() {
        return cardNumber;
    }

    public Date getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(Date expireDate) {
        this.expireDate = expireDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public List<Transaction> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transaction> transactions) {
        this.transactions = transactions;
    }

    public void setCardNumber(long cardNumber) {
        this.cardNumber = cardNumber;
    }


}
