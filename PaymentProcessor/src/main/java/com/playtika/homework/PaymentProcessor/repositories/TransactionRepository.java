package com.playtika.homework.PaymentProcessor.repositories;

import com.playtika.homework.PaymentProcessor.models.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.Date;
import java.util.List;

public interface TransactionRepository extends JpaRepository<Transaction, Long> {

    @Query(value = "SELECT * FROM transactions WHERE transactions.transaction_status LIKE :status", nativeQuery = true)
    List<Transaction> getTransactionsByStatus(String status);

    @Query(value = "SELECT * FROM transactions WHERE transactions.transaction_date >= :startDate AND transactions.transaction_date <= :endDate", nativeQuery = true)
    List<Transaction> getTransactionInDateInterval(String startDate, String endDate);

    @Query(value = "SELECT * FROM transactions WHERE transactions.id = :cardId", nativeQuery = true)
    List<Transaction> getTransactionsByCardNumber(long cardId);

    @Query(value = "SELECT * FROM transactions WHERE transactions.id = :cardId", nativeQuery = true)
    List<Transaction> getTransactionsByUserCNP(long cardId);

    @Query(value = "SELECT id FROM transactions WHERE transactions.id = :transactionId", nativeQuery = true)
    long getCardIdForTransaction(long transactionId);
}
