package com.playtika.homework.PaymentProcessor.controllers;

import com.playtika.homework.PaymentProcessor.models.Address;
import com.playtika.homework.PaymentProcessor.models.User;
import com.playtika.homework.PaymentProcessor.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/users")
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping
    public List<User> getAllUsers() {
        return this.userService.getAllUsers();
    }

    @DeleteMapping
    @RequestMapping("/delete/{id}")
    public boolean deleteUser(@PathVariable long id) {
        return this.userService.deleteUser(id);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<User> createNewUser(@RequestBody User user) {
        if(this.userService.findById(user.getId()) != null) {
            return new ResponseEntity<User>(user, HttpStatus.NO_CONTENT);
        }

        User createdUser = userService.addUser(user);
        return new ResponseEntity<User>(createdUser, HttpStatus.NO_CONTENT);
    }

    @PutMapping
    public ResponseEntity<Boolean> updateUser(@RequestBody User user) {
        if(this.userService.findById(user.getId()) == null) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        }

        this.userService.updateUser(user);
        return new ResponseEntity<>(true, HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("{id}/add/address")
    public @ResponseBody User addUserAddress(@PathVariable long id, @RequestBody Address address) {
        return this.userService.addAddress(id, address);
    }

    @PostMapping
    @RequestMapping(value = "/{userId}/addcard", params = {"cardId"})
    public User addCard(@PathVariable long userId, @RequestParam long cardId){
        return this.userService.addCard(userId, cardId);
    }
}
