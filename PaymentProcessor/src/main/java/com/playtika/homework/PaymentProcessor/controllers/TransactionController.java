package com.playtika.homework.PaymentProcessor.controllers;

import com.playtika.homework.PaymentProcessor.models.Transaction;
import com.playtika.homework.PaymentProcessor.services.TransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.persistence.Table;
import java.util.List;

@RestController
@RequestMapping("/api/transactions")
public class TransactionController {

    @Autowired
    TransactionService transactionService;

//    @GetMapping
//    @RequestMapping("/initialize")
//    public int initializeTransaction() {
//        return this.transactionService.initializeTransaction();
//    }


    @GetMapping
    @RequestMapping("/{id}")
    public Transaction getTransactionById(@PathVariable long id) {
        Transaction transaction = this.transactionService.getTransactionById(id);
        if(transaction != null) {
            return transaction;
        }

        return null;
    }

    @GetMapping
    @RequestMapping(params = {"status"})
    public List<Transaction> getTransactionsByStatus(@RequestParam String status) {
        List<Transaction> transactions = this.transactionService.getTransactionsByStatus(status);
        if(transactions != null) {
            return transactions;
        }

        return null;
    }

    @GetMapping
    @RequestMapping(params = {"cardNumber"})
    public List<Transaction> getTransactionsByCardNumber(@RequestParam long cardNumber) {
        List<Transaction> transactions = this.transactionService.getTransactionsByCardNumber(cardNumber);
        if(transactions != null) {
            return transactions;
        }

        return null;
    }

    @GetMapping
    @RequestMapping(params = {"CNP"})
    public List<List<Transaction>> getTransactionByUserCnp(@RequestParam long CNP) {
        List<List<Transaction>> transactions = this.transactionService.getTransactionByUserCnp(CNP);
        if(transactions != null) {
            return transactions;
        }

        return null;
    }

    @GetMapping
    @RequestMapping(params = {"startDate", "endDate"})
    public List<Transaction> getTransactionsInDateInterval(@RequestParam String startDate, @RequestParam String endDate) {
        return this.transactionService.getTransactionInDateInterval(startDate, endDate);
    }

    @PostMapping
    @RequestMapping("/authorize/{id}")
    public Transaction authorizeTransaction(@PathVariable long id) {
        return this.transactionService.authorizeTransaction(id);
    }

    @PostMapping
    @RequestMapping("/capture/{id}")
    public Transaction captureTransaction(@PathVariable long id) {
        return this.transactionService.captureTransaction(id);
    }

    @PostMapping
    @RequestMapping("/refund/{id}")
    public Transaction refundTransaction(@PathVariable long id) {
        return this.transactionService.refundTransaction(id);
    }

    @PostMapping
    @RequestMapping("/cancel/{id}")
    public Transaction cancelTransaction(@PathVariable long id) {
        return this.transactionService.cancelTransaction(id);
    }

}
