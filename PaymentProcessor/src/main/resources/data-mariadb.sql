USE paymentprocessor;

INSERT INTO users (
    user_cnp,
    user_firstname,
    user_lastname
)
VALUES
    (192392132, 'Iulian', 'Tcaciuc'),
    (121312312, 'Iulian', 'Marius'),
    (123123132, 'Iulian', 'Iulian'),
    (123812832, 'Marius', 'Marius');


INSERT INTO cards (
    card_number,
    card_expire_date,
    id
)
VALUES
    (123, '2020-03-02', 1),
    (124, '2020-03-01', 2),
    (125, '2020-05-04', 3),
    (126, '2020-01-05', 4),
    (127, '2020-09-02', 2);


INSERT INTO transactions (
    transaction_amount,
    transaction_captured_date,
    transaction_refund_date,
    transaction_status,
    transaction_date,
    id
)

VALUES
    (300, '2020-03-02', '2020-04-02', 'INITIATED', '2020-02-02', 1),
    (300, '2020-03-02', '2020-04-02', 'CAPTURED', '2020-02-02', 2),
    (300, '2020-03-02', '2020-04-02', 'CANCEL', '2020-02-01', 3),
    (300, '2020-03-02', '2020-04-02', 'AUTHORIZED', '2020-02-02', 4),
    (300, '2020-03-02', '2020-04-02', 'REFUNDED', '2020-02-02', 3);