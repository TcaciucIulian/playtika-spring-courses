import com.playtika.java.training.challenge3.tcaciuc.iulian.composite.Email;
import com.playtika.java.training.challenge3.tcaciuc.iulian.composite.EmailAccount;
import com.playtika.java.training.challenge3.tcaciuc.iulian.composite.EmailGroup;
import org.junit.Test;
import static org.assertj.core.api.Assertions.*;

public class EmailTests {

    @Test
    public void UnsuportedEmailException() {
        EmailGroup emailGroup = new EmailGroup("Email group");
        assertThatThrownBy(() -> emailGroup.addEmail(null))
                .isInstanceOf(Exception.class);
    }

    @Test
    public void GetValidEmailToGroupTest() {
        Email emailGroup = new EmailGroup("Email group");
        EmailAccount emailAccount = new EmailAccount("iulina@yahoo.com", "Iulian", "Marius", "eu");
        emailGroup.addEmail(emailAccount);
        Email emailAccount1 = emailGroup.getEmail(0);
        assertThat(emailAccount1 != null);
    }

    @Test
    public void GetInvalidEmailIndexToGroupTest() {
        Email emailGroup = new EmailGroup("Email group");
        EmailAccount emailAccount = new EmailAccount("iulina@yahoo.com", "Iulian", "Marius", "eu");
        emailGroup.addEmail(emailAccount);

        assertThatThrownBy(() -> emailGroup.getEmail(5))
                .isInstanceOf(Exception.class);
    }

    @Test
    public void RemoveValidEmailTest() {
        Email emailGroup = new EmailGroup("Email group");
        EmailAccount emailAccount = new EmailAccount("iulina@yahoo.com", "Iulian", "Marius", "eu");
        emailGroup.addEmail(emailAccount);
        emailGroup.removeEmail(emailAccount);

        assertThatThrownBy(() -> emailGroup.getEmail(0))
                .isInstanceOf(Exception.class);

    }

    @Test
    public void RemoveInvalidEmailTest() {
        Email emailGroup = new EmailGroup("Email group");
        assertThatThrownBy(() -> emailGroup.removeEmail(null))
                .isInstanceOf(Exception.class);
    }

}
