import com.playtika.java.training.challenge3.tcaciuc.iulian.factory.CloudEmailServer;
import com.playtika.java.training.challenge3.tcaciuc.iulian.factory.EmailServer;
import com.playtika.java.training.challenge3.tcaciuc.iulian.factory.EmailServerCreator;
import com.playtika.java.training.challenge3.tcaciuc.iulian.factory.OnPremiseEmailServer;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;
import org.mockito.Mockito;

public class ServerTests {

    @Test
    public void testDataForCloudEmailServer() {
        EmailServer emailServer = EmailServerCreator.createNewServer("C");
        assertThatObject(emailServer).isInstanceOf(CloudEmailServer.class);
    }

    @Test
    public void testDataForPremiseEmailServer() {
        EmailServer emailServer = EmailServerCreator.createNewServer("P");
        assertThatObject(emailServer).isInstanceOf(OnPremiseEmailServer.class);
    }

    @Test
    public void testDataForInvalidServer() {
        EmailServer emailServer = EmailServerCreator.createNewServer("");
        assertThatObject(emailServer).isNull();
    }

    @Test
    public void testSingletonForCloudEmailServer() {
        EmailServer emailServer = EmailServerCreator.createOrGetExistant("C");
        EmailServer emailServer1 = EmailServerCreator.createOrGetExistant("C");
        assertThat(emailServer == emailServer1).isTrue();
    }

    @Test
    public void testSingletonForOnPremiseEmailServer() {
        EmailServer emailServer = EmailServerCreator.createOrGetExistant("P");
        EmailServer emailServer1 = EmailServerCreator.createOrGetExistant("P");
        assertThat(emailServer == emailServer1).isTrue();
    }

}
