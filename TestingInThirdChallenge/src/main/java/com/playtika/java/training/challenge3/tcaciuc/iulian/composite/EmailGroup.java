package com.playtika.java.training.challenge3.tcaciuc.iulian.composite;


import java.util.ArrayList;
import java.util.List;

public class EmailGroup implements Email{
    private String name;
    private List<Email> children;

    public EmailGroup(String name) {
        this.name = name;
        children = new ArrayList<>();
    }


    @Override
    public void printDetails() {
        System.out.println("    Email group " + name);
        for(Email email : children) {
            email.printDetails();
        }
    }

    @Override
    public void addEmail(Email email) {
        if(email != null) {
            children.add(email);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    @Override
    public void removeEmail(Email email) {
        if(email == null || !children.contains(email)) {
            throw new UnsupportedOperationException();
        }

        children.remove(email);
    }

    @Override
    public Email getEmail(Integer index) {
        if(index >= 0 && index < children.size()) {
            return children.get(index);
        }
        throw new UnsupportedOperationException();
    }

    @Override
    public void receive(Message message) {
        message.getReceiver().printDetails();
        System.out.println("Mesaj : " + message.getMessage());
    }


}
