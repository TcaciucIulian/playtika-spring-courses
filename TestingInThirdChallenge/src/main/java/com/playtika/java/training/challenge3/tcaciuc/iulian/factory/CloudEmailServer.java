package com.playtika.java.training.challenge3.tcaciuc.iulian.factory;


import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;


public class CloudEmailServer implements EmailServer {
    private long upTime;
    private LocalDateTime startTime;

    CloudEmailServer() {}


    @Override
    public void start() {
        startTime = LocalDateTime.now();
        System.out.println("The cloud email server C was started at " + startTime);
    }

    @Override
    public void stop() {
        upTime = startTime.until(LocalDateTime.now(), ChronoUnit.SECONDS);
        System.out.println("The cloud email server stopped after " + upTime + " seconds");
    }

    @Override
    public void execute() {
        stop();
        start();
    }


}
