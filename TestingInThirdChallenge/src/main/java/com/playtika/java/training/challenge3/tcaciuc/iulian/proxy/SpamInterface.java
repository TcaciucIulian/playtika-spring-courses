package com.playtika.java.training.challenge3.tcaciuc.iulian.proxy;

public interface SpamInterface {
    boolean isSpam(String message);
}
