package com.playtika.java.training.challenge3.tcaciuc.iulian.repository;

import java.util.List;
import java.util.Vector;

public class MessagesFileRepo<T> implements Repository<T> {
    private String fileName;
    private List<T> entries = new Vector<>();

    @Override
    public void insertMessage(T entry) {
        entries.add(entry);
    }

    @Override
    public void deleteMessage(Integer id) {
        entries.remove(entries.get(id));
    }

    @Override
    public List<T> getAllReceivedMessages(String email) {
        return null;
    }

    @Override
    public List<T> getAllStringBodyMessages(String keyword) {
        return null;
    }

    @Override
    public List<T> getAllSentMessages(String email) {
        return null;
    }


}
