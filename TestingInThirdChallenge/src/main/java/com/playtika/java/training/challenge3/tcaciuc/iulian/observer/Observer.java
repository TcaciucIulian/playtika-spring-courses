package com.playtika.java.training.challenge3.tcaciuc.iulian.observer;

public interface Observer {
    void update();
}
