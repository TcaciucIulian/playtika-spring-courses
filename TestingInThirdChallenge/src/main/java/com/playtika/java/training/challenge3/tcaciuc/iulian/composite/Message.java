package com.playtika.java.training.challenge3.tcaciuc.iulian.composite;


import com.playtika.java.training.challenge3.tcaciuc.iulian.proxy.SpamFilter;
import com.playtika.java.training.challenge3.tcaciuc.iulian.proxy.SpamInterface;

public class Message {
    private Email sender;
    private Email receiver;
    private String message;
    private SpamInterface spamInterface = new SpamFilter();

    public Message(Email sender, Email receiver, String message) {
        this.sender = sender;
        this.receiver = receiver;
        this.message = message;
    }

    public void send() {
        if(spamInterface.isSpam(this.getMessage())) {
            this.message = "SPAM!!!" + this.getMessage();
        }
        receiver.receive(this);
    }

    public Email getSender() {
        return sender;
    }

    public Email getReceiver() {
        return receiver;
    }

    public String getMessage() {
        return message;
    }


}
