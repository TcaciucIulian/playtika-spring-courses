package com.playtika.java.training.challenge3.tcaciuc.iulian.proxy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SpamFilter implements SpamInterface{
    private List<String> avoidedStrings = new ArrayList<String>(Arrays.asList("win", "buy", "nigerian prince"));

    @Override
    public boolean isSpam(String message) {
        if(checkNumbersInMessage(message)) {
            return true;
        }

        if(checkSpamKeywordsInMessage(message)) {
            return true;
        }

        return false;

    }
    public boolean checkSpamKeywordsInMessage(String message) {
        String[] words = message.split(" ");
        for(String word : words) {
            if(avoidedStrings.contains(word)) {
                return true;
            }
        }
        return false;
    }

    public boolean checkNumbersInMessage(String message) {
        Pattern p = Pattern.compile("\\d+");
        Matcher m = p.matcher(message);
        while(m.find()) {
            if(Integer.parseInt(m.group()) > 5000) {
                return true;
            }
        }
        return false;
    }
}
