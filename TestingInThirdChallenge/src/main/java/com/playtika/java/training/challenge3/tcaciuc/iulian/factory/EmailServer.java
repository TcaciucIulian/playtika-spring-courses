package com.playtika.java.training.challenge3.tcaciuc.iulian.factory;


public interface EmailServer {
     void start();
     void stop();
     void execute();
}
