package com.playtika.java.training.challenge3.tcaciuc.iulian.repository;

import java.util.List;
import java.util.function.Predicate;

public interface Repository<T> {
    void insertMessage(T entry);
    void deleteMessage(Integer id);
    List<T> getAllSentMessages(String email);
    List<T> getAllReceivedMessages(String email);
    List<T> getAllStringBodyMessages(String keyword);
}
