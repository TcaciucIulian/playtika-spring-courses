package com.playtika.java.training.challenge3.tcaciuc.iulian.servicelocator;

import java.util.HashMap;
import java.util.Map;

public class ServiceLocator {
    private static Map<Class<?>, Object> map = new HashMap<>();

    public static void register(Class<?> contract, Object implementation){
        map.put(contract, implementation);
    }

    public static Object resolve(Object contract) {
        return map.get(contract);
    }
}
