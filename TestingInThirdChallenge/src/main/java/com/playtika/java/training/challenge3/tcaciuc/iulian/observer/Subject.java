package com.playtika.java.training.challenge3.tcaciuc.iulian.observer;

public interface Subject {
    void subscribe(Observer observer);
    void unsubscribe(Observer observer);
    void notifyObservers(Observer observer);
}
