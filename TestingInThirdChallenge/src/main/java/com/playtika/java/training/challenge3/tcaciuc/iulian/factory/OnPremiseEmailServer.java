package com.playtika.java.training.challenge3.tcaciuc.iulian.factory;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class OnPremiseEmailServer implements EmailServer {
    private LocalDateTime startTime;
    private long upTime;

    OnPremiseEmailServer() {}


    @Override
    public void start() {
        startTime = LocalDateTime.now();
        System.out.println("The server P was start at " + " " + String.valueOf(startTime));
    }

    @Override
    public void stop() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        upTime = startTime.until(LocalDateTime.now(), ChronoUnit.SECONDS);
        System.out.println("On premise email server stopped after " + upTime + " seconds");
    }

    @Override
    public void execute() {
        stop();
        start();
    }


}
