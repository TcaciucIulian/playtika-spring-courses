package com.playtika.java.training.challenge3.tcaciuc.iulian.composite;

import com.playtika.java.training.challenge3.tcaciuc.iulian.observer.Observer;
import com.playtika.java.training.challenge3.tcaciuc.iulian.observer.ServiceMonitor;
import com.playtika.java.training.challenge3.tcaciuc.iulian.observer.Subject;

public class EmailAccount implements Email, Subject {
    private String emailAdress;
    private String name;
    private String surname;
    private String signature;
    private Observer observer = new ServiceMonitor();

    public EmailAccount(String emailAdress, String name, String surname, String signature) {
        this.emailAdress = emailAdress;
        this.name = name;
        this.surname = surname;
        this.signature = signature;
    }

    public void receive(Message message) {
        message.getReceiver().printDetails();
        System.out.println(" MESAJ : "  + message.getMessage());
        notifyObservers(observer);
    }

    @Override
    public void printDetails() {
        System.out.println("        EmailAccount " + name);
    }

    @Override
    public void addEmail(Email email) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void removeEmail(Email email) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Email getEmail(Integer index) {
        throw  new UnsupportedOperationException();
    }


    @Override
    public void subscribe(Observer observer) {
        this.observer = observer;
    }

    @Override
    public void unsubscribe(Observer observer) {
        this.observer = null;
    }

    @Override
    public void notifyObservers(Observer observer) {
        if(observer != null) {
            observer.update();
        }
    }
}
