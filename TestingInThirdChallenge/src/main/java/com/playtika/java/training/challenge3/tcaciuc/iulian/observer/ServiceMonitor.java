package com.playtika.java.training.challenge3.tcaciuc.iulian.observer;
import org.mockito.Mockito;
import com.playtika.java.training.challenge3.tcaciuc.iulian.factory.EmailServer;
import com.playtika.java.training.challenge3.tcaciuc.iulian.factory.EmailServerCreator;

public class ServiceMonitor implements Observer{
    private static int count = 0;
    EmailServer emailServer = EmailServerCreator.createOrGetExistant("C");


    public void incrementCount() {
        count++;
    }

    @Override
    public void update() {
        incrementCount();
        if(count >= 20) {
            System.out.println("Reaches 20");
            emailServer.execute();
            count = 0;
        }
    }
}
