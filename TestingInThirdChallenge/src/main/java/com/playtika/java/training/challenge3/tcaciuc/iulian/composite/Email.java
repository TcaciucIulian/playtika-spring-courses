package com.playtika.java.training.challenge3.tcaciuc.iulian.composite;

public interface Email {
    void printDetails();
    void addEmail(Email email);
    void removeEmail(Email email);
    Email getEmail(Integer index);
    void receive(Message message);

}
