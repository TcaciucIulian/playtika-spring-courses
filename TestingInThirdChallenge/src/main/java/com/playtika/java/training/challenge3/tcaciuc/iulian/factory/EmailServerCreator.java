package com.playtika.java.training.challenge3.tcaciuc.iulian.factory;


import java.util.ArrayList;
import java.util.List;

public class EmailServerCreator {

    private static List<EmailServer> instances;
    private static String[] typeOfInstances = new String[]{"C", "P"};
    private static EmailServer instance = null;


    static {
        instances = new ArrayList<>();
        for(int i = 0; i < typeOfInstances.length; i++) {
            String typeOfInstance = typeOfInstances[i];
            instances.add(createNewServer(typeOfInstance));
        }
    }

    public EmailServerCreator() {
    }


    public static EmailServer createOrGetExistant(String server) {
        for(EmailServer inst : instances) {
            if(server.equals("P")) {
                if(inst instanceof OnPremiseEmailServer) {
                    instance = inst;
                }
            }

            if(server.equals("C")) {
                if(inst instanceof CloudEmailServer) {
                    instance = inst;
                }
            }

        }
        return instance;
    }

    public static EmailServer createNewServer(String server) {
        if(server.equals("P")) {
            return new OnPremiseEmailServer();
        }

        if(server.equals("C")) {
            return new CloudEmailServer();
        }
        return null;
    }


}
