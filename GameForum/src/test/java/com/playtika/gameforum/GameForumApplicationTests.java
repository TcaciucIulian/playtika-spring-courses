package com.playtika.gameforum;


import com.playtika.gameforum.repositories.PlayerRepository;
import com.playtika.gameforum.services.PlayerService;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.junit.runner.Runner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import javax.swing.*;

@RunWith(SpringRunner.class)
@SpringBootTest(
        webEnvironment = SpringBootTest.WebEnvironment.MOCK,
        classes = GameForumApplication.class
)
@AutoConfigureMockMvc
class GameForumApplicationTests {

    @Autowired
    PlayerService playerService;
    @Autowired
    PlayerRepository playerRepository;

    @Autowired
    private MockMvc mvc;


    @Test
    void testPlayersEndpointURL() {
        try {
            mvc.perform(get("http://localhost:5050/api/players")).andExpect(status().isOk());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    void testPlayersGetById() {
        try {
            mvc.perform(get("http://localhost:5050/api/players/1")).andExpect(status().isOk());
        }catch (Exception e) {
            e.printStackTrace();
        }
    }
//    @Test
//    void contextLoads() {
//    }

}
