package com.playtika.gameforum.actuator;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.actuate.endpoint.annotation.Endpoint;
import org.springframework.boot.actuate.info.Info;
import org.springframework.boot.actuate.info.InfoContributor;
import org.springframework.stereotype.Component;

import javax.persistence.Entity;
import java.util.HashMap;
import java.util.Map;

@Component
public class InfoContributorVersion implements InfoContributor {

    @Value("${version}")
    public String version;
    @Value("${description}")
    public String description;

    @Override
    public void contribute(Info.Builder builder) {
        Map<String, Object> properties = new HashMap<>();
        properties.put("version",this.version);
        properties.put("description", this.description);

        builder.withDetails(properties);
    }
}
