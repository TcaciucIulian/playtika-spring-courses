package com.playtika.gameforum.controllers;


import com.playtika.gameforum.exceptions.PlayerNotFoundException;
import com.playtika.gameforum.exceptions.ResponseBody;
import com.playtika.gameforum.models.Player;
import com.playtika.gameforum.services.PlayerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/players")
public class PlayerController {

    @Autowired
    PlayerService playerService;


    @GetMapping
    public List<Player> getAll() {
        return this.playerService.findAll();
    }

    @GetMapping
    @RequestMapping("/sorted")
    public List<Player> getAllSorted() {
        return this.playerService.finaAllSortedByName();
    }


    @GetMapping
    @RequestMapping("/{id}")
    public Player getPlayerById(@PathVariable long id) {
        Player player = this.playerService.findById(id);
        if(player != null) {
            return player;
        }

        throw new PlayerNotFoundException();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<Player> createNewPlayer(@RequestBody Player player) {
        if(this.playerService.findById(player.getId()) != null) {
            return new ResponseEntity<Player>(player, HttpStatus.NO_CONTENT);
        }

        Player createdPlayer = playerService.createPlayer(player);
        return new ResponseEntity<Player>(createdPlayer, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping
    @RequestMapping("/delete/{id}")
    public boolean deletePlayer(@PathVariable long id) {
        return this.playerService.deleteById(id);
    }

    // Putem face un enum cu strngurile si factory
    @ExceptionHandler
    @ResponseStatus
    public ResponseEntity<ResponseBody> handleExceptions(Exception exception) {
        if(exception instanceof PlayerNotFoundException) {
//            return new ControllerGenericExceptionMessage("Player not found", HttpStatus.NOT_FOUND);
            return new ResponseEntity<ResponseBody>(new ResponseBody("Player not found", HttpStatus.NOT_FOUND.value()), HttpStatus.NOT_FOUND);
        }

        return new ResponseEntity<ResponseBody>(new ResponseBody("Houston we have a problem", -1), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @PutMapping
    public ResponseEntity<Boolean> updatePlayer(@RequestBody Player player) {
        if(this.playerService.findById(player.getId()) == null) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        } else {
            this.playerService.updatePlayer(player);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
    }
}
