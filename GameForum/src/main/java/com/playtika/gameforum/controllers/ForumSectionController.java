package com.playtika.gameforum.controllers;

import com.playtika.gameforum.exceptions.PlayerNotFoundException;
import com.playtika.gameforum.exceptions.ResponseBody;
import com.playtika.gameforum.models.ForumSection;
import com.playtika.gameforum.models.Player;
import com.playtika.gameforum.models.PlayerSection;
import com.playtika.gameforum.services.ForumSectionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/forumsections")
public class ForumSectionController {

    @Autowired
    ForumSectionService forumSectionService;

    @GetMapping
    public List<ForumSection> getAll() {
        return this.forumSectionService.findAll();
    }



    @GetMapping
    @RequestMapping("/{id}")
    public ForumSection getPlayerById(@PathVariable long id) {
        ForumSection forumSection = this.forumSectionService.findById(id);
        if(forumSection != null) {
            return forumSection;
        }

        throw new PlayerNotFoundException();
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ForumSection> createNewPlayer(@RequestBody ForumSection forumSection) {
        if(this.forumSectionService.findById(forumSection.getId()) != null) {
            return new ResponseEntity<ForumSection>(forumSection, HttpStatus.NO_CONTENT);
        }

        ForumSection createdForumSection = forumSectionService.createForumSection(forumSection);
        return new ResponseEntity<ForumSection>(createdForumSection, HttpStatus.NO_CONTENT);
    }

    @DeleteMapping
    @RequestMapping("/delete/{id}")
    public boolean deletePlayer(@PathVariable long id) {
        return this.forumSectionService.deleteById(id);
    }

    @PostMapping
    @RequestMapping("/{id_section}/register/{id_player}")
    public boolean registerPlayer(@PathVariable long id_section, @PathVariable long id_player) {
        PlayerSection playerSection = new PlayerSection();
        playerSection.setPlayerId(id_player);
        playerSection.setSectionId(id_section);
        return forumSectionService.registerPlayerIntoSection(playerSection);
    }

    @PutMapping
    @RequestMapping("/register")
    public boolean registerPlayer(@RequestBody PlayerSection playerSection) {
        return forumSectionService.registerPlayerIntoSection(playerSection);
    }


    @PutMapping
    public ResponseEntity<Boolean> updatePlayer(@RequestBody ForumSection forumSection) {
        if(this.forumSectionService.findById(forumSection.getId()) == null) {
            return new ResponseEntity<>(false, HttpStatus.NOT_FOUND);
        } else {
            this.forumSectionService.updatePlayer(forumSection);
            return new ResponseEntity<>(true, HttpStatus.OK);
        }
    }

}
