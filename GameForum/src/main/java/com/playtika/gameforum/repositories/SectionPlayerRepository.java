package com.playtika.gameforum.repositories;

import com.playtika.gameforum.models.ForumSection;
import com.playtika.gameforum.models.PlayerSection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SectionPlayerRepository extends JpaRepository<PlayerSection, ForumSection> {
}
