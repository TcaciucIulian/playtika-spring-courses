package com.playtika.gameforum.repositories;

import com.playtika.gameforum.models.ForumSection;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ForumSectionRepository extends JpaRepository<ForumSection, Long> {
}
