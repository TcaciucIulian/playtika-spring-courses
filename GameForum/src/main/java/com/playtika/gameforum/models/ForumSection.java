package com.playtika.gameforum.models;

import javax.persistence.*;
import java.util.List;


@Entity(name = "forum_sections")
public class ForumSection {

    @Id
    @Column(name = "section_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "section_name")
    private String name;

    @Column(name = "section_description")
    private String description;

    @Column(name = "section_players_limit")
    private String playersLimit;

    @ManyToMany
    @JoinTable(
            name = "section_players",
            joinColumns = @JoinColumn(name = "section_id", referencedColumnName = "section_id"),
            inverseJoinColumns = @JoinColumn(name = "player_id", referencedColumnName = "id")
    )
    List<Player> players;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPlayersLimit() {
        return playersLimit;
    }

    public void setPlayersLimit(String playersLimit) {
        this.playersLimit = playersLimit;
    }

    public List<Player> getPlayers() {
        return players;
    }
}
