package com.playtika.gameforum.models;

import javax.persistence.Column;
import javax.persistence.Id;
import java.io.Serializable;

public class PlayerSectionID implements Serializable {

    private long playerId;
    private long sectionId;

    public PlayerSectionID() {

    }

    public PlayerSectionID(long playerId, long sectionId) {
        this.playerId = playerId;
        this.sectionId = sectionId;
    }

    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public long getSectionId() {
        return sectionId;
    }

    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }
}
