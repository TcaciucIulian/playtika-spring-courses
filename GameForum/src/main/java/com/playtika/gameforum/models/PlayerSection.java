package com.playtika.gameforum.models;

import javax.persistence.*;

@Entity
@Table(name = "section_players")
@IdClass(PlayerSectionID.class)
public class PlayerSection {

    @Id
    @Column(name = "player_id")
    private long playerId;

    @Id
    @Column(name = "section_id")
    private long sectionId;


    public long getPlayerId() {
        return playerId;
    }

    public void setPlayerId(long playerId) {
        this.playerId = playerId;
    }

    public long getSectionId() {
        return sectionId;
    }

    public void setSectionId(long sectionId) {
        this.sectionId = sectionId;
    }
}
