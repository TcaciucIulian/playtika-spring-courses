package com.playtika.gameforum.services;

import com.playtika.gameforum.models.ForumSection;
import com.playtika.gameforum.models.Player;
import com.playtika.gameforum.models.PlayerSection;
import com.playtika.gameforum.repositories.ForumSectionRepository;
import com.playtika.gameforum.repositories.SectionPlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ForumSectionService {

    @Autowired
    ForumSectionRepository forumSectionRepository;

    @Autowired
    SectionPlayerRepository sectionPlayerRepository;


    public boolean registerPlayerIntoSection(PlayerSection playerSection) {
        this.sectionPlayerRepository.saveAndFlush(playerSection);
        return true;
    }

    public List<ForumSection> findAll() {
        return this.forumSectionRepository.findAll();
    }

    public boolean deleteById(long id) {
        if(this.forumSectionRepository.findById(id).isPresent()) {
            return true;
        }
        return false;
    }

    public ForumSection findById(long id) {
        Optional<ForumSection> forumSection = this.forumSectionRepository.findById(id);
        if(forumSection.isPresent()) {
            return forumSection.get();
        }
        return null;
    }

    public ForumSection createForumSection(ForumSection forumSection) {

        ForumSection newForumSection = this.forumSectionRepository.saveAndFlush(forumSection);
        return newForumSection;
    }

    public boolean updatePlayer(ForumSection forumSection) {
        Optional<ForumSection> existingForum = this.forumSectionRepository.findById(forumSection.getId());
        if(existingForum.isPresent()) {
//            Player existing = existingPlayer.get();
//            existing.setLastName(player.getLastName());
//            existing.setFirstName(player.getFirstName());
//            existing.setUserName(player.getUserName());
//            existing.setBio(player.getBio());
//            BeanUtils.copyProperties(existing, player);
            this.forumSectionRepository.saveAndFlush(forumSection);
            return true;
        }

        return false;
    }
}
