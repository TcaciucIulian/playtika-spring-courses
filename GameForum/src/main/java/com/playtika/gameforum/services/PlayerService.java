package com.playtika.gameforum.services;

import com.playtika.gameforum.models.Player;
import com.playtika.gameforum.repositories.PlayerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class PlayerService {

    @Autowired
    PlayerRepository playerRepository;

    public List<Player> findAll() {
        return this.playerRepository.findAll();
    }

    public List<Player> finaAllSortedByName(){
        return this.playerRepository.findAll(Sort.by(Sort.Direction.DESC, "lastName"));
//        return this.playerRepository.findAll().sort((x, y) -> {
//            return x.getLastName().compareTo(y.getLastName())
//        });
    }

    public Player findById(long id) {
        Optional<Player> player = this.playerRepository.findById(id);
        if(player.isPresent()) {
            return player.get();
        }
        return null;
    }

    public boolean deleteById(long id) {
        if(this.playerRepository.findById(id).isPresent()) {
            this.playerRepository.deleteById(id);
            return true;
        }
        return false;
    }

    public Player createPlayer(Player player) {

        Player newPlayer = this.playerRepository.saveAndFlush(player);
        return newPlayer;

    }

    public boolean updatePlayer(Player player) {
        Optional<Player> existingPlayer = this.playerRepository.findById(player.getId());
        if(existingPlayer.isPresent()) {
//            Player existing = existingPlayer.get();
//            existing.setLastName(player.getLastName());
//            existing.setFirstName(player.getFirstName());
//            existing.setUserName(player.getUserName());
//            existing.setBio(player.getBio());
//            BeanUtils.copyProperties(existing, player);
            this.playerRepository.saveAndFlush(player);
            return true;
        }

        return false;
    }
}
