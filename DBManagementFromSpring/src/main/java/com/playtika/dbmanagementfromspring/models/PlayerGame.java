package com.playtika.dbmanagementfromspring.models;

import javax.persistence.*;

@Entity
@Table(name = "player_games")
public class PlayerGame {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(cascade = CascadeType.REMOVE)
    @JoinColumn(name = "player_id",referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    private Player player;


    private String game;

    @Column(name = "total_points")
    private int totalPoints;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGame() {
        return game;
    }

    public void setGame(String game) {
        this.game = game;
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public void setTotalPoints(int totalPoints) {
        this.totalPoints = totalPoints;
    }
}
