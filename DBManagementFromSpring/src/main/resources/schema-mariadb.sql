CREATE OR REPLACE TABLE `forum_sections` (
                                  `section_id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
                                  `section_name` VARCHAR(80) NOT NULL COLLATE 'latin1_swedish_ci',
                                  `section_description` VARCHAR(1024) NOT NULL COLLATE 'latin1_swedish_ci',
                                  `section_players_limit` INT(11) NOT NULL,
                                  PRIMARY KEY (`section_id`) USING BTREE
)
    COLLATE='latin1_swedish_ci'
    ENGINE=InnoDB
    AUTO_INCREMENT=6
;
