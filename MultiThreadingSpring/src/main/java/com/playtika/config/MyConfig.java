package com.playtika.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

@Component
@Configuration
public class MyConfig {

    @Value("${no-cores}")
    public int noCores;

    @Value("${no-max-threads}")
    public int noMaxThreads;

    public int getNoCores() {
        return noCores;
    }

    public int getNoMaxThreads() {
        return noMaxThreads;
    }

    public void setNoCores(int noCores) {
        this.noCores = noCores;
    }

    public void setNoMaxThreads(int noMaxThreads) {
        this.noMaxThreads = noMaxThreads;
    }
}
