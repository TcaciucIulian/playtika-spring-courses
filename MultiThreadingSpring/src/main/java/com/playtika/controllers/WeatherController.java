package com.playtika.controllers;

import com.playtika.models.User;
import com.playtika.models.WeatherData;
import com.playtika.services.GitHubService;
import com.playtika.services.WeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

@RestController
@RequestMapping("/api/weather")
public class WeatherController {

    @Autowired
    WeatherService weatherService;
    @Autowired
    GitHubService gitHubService;

    @GetMapping
    @RequestMapping("{location}")
    public WeatherData getWeatherData(@PathVariable String location) throws ExecutionException, InterruptedException {
        try {
            Future<WeatherData> future = weatherService.getWeatherData(location);
            while(true) {
                if(future.isDone()) {
                    return future.get();
                } else {
                    //do other stuff
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return new WeatherData("", 0);

    }


    @GetMapping
    @RequestMapping("/github/{username}")
    public User getGitHubUser(@PathVariable String username) {
        try {
            return gitHubService.getUserData(username).get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        return null;
    }
}
