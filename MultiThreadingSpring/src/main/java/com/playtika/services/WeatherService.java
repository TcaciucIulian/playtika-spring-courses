package com.playtika.services;

import com.playtika.models.WeatherData;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.Future;

@Service
public class WeatherService {

    @Async
    public Future<WeatherData> getWeatherData(String location) {
        System.out.println("Getting data for " + location);
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Thread name" + Thread.currentThread().getName());
        System.out.println("Returning data");
        WeatherData weather = new WeatherData("It's sunny", 20);
        return CompletableFuture.completedFuture(weather);
    }
}
