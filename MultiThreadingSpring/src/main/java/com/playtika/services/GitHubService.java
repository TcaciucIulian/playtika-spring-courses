package com.playtika.services;

import com.playtika.models.User;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.concurrent.CompletableFuture;

@Service
public class GitHubService {

    RestTemplate restTemplate;
    public static final String URL = "https://api.github.com/users/";

    public GitHubService(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build(); // asa obtinem clientul de REST
    }

    @Async
    public CompletableFuture<User> getUserData(String userName) {

        User userData = restTemplate.getForObject(URL + " " + userName, User.class);

        return CompletableFuture.completedFuture(userData);
    }

}
