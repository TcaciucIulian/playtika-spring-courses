package com.playtika.models;

import com.fasterxml.jackson.annotation.JsonProperty;

public class User {

    @JsonProperty("login")
    private String login;
    @JsonProperty("avatar_url")
    private String avatarUrl;

    private String url;


    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }
}
