package com.playtika.multithreadingspring;

import com.playtika.config.MyConfig;
import org.springframework.aop.interceptor.AsyncUncaughtExceptionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.AsyncConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Component;

import java.util.concurrent.Executor;


@ComponentScan("com.playtika")
@SpringBootApplication
public class MultiThreadingSpringApplication implements AsyncConfigurer {

    @Autowired
    private MyConfig myConfig;

    public static void main(String[] args) {

        SpringApplication.run(MultiThreadingSpringApplication.class, args);
    }


    @Override
    public Executor getAsyncExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(myConfig.noCores);
        executor.setMaxPoolSize(myConfig.noMaxThreads);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("MTTest");
        executor.initialize();
        return executor;
    }

    @Override
    public AsyncUncaughtExceptionHandler getAsyncUncaughtExceptionHandler() {
        return AsyncConfigurer.super.getAsyncUncaughtExceptionHandler();
    }
}
